package model

import java.nio.file.StandardOpenOption
import kotlinx.serialization.json.Json
import java.nio.file.Path
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlin.io.path.readText
import kotlin.io.path.writeText

@Serializable
class FilmItbStorage {

    val userDataPath = Path.of("data/userData.json")
    val filmDataPath = Path.of("data/filmData.json")


    fun save(userData: MutableList<User>, filmData: MutableList<Film>){
        userDataPath.writeText(Json.encodeToString(userData))
        filmDataPath.writeText(Json.encodeToString(filmData))
    }
    fun loadUsers(): MutableList<User> {
        val users = Json.decodeFromString<MutableList<User>>(userDataPath.readText())
        return users
    }

    fun loadFilms(): MutableList<Film> {
        val films = Json.decodeFromString<MutableList<Film>>(filmDataPath.readText())
        return films
    }


}




