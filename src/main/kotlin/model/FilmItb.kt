package model

data class FilmItb (var filmlist :MutableList<Film> = mutableListOf(),
               var userlist :MutableList<User> = mutableListOf())
{

    val filmItbStorage = FilmItbStorage()
    init {

        filmlist = filmItbStorage.loadFilms()
        userlist = filmItbStorage.loadUsers()
    }

    fun showFilms(): String? {
        return if (filmlist.isEmpty()) {
            null
        } else {
            userFriendlyFilmsResult()
        }
    }

    fun userFriendlyFilmsResult(): String {
        var filmsToReturn = ""
        for (i in 0 until filmlist.lastIndex) {
            filmsToReturn += "${filmlist[i]}\n"
        }
        filmsToReturn += filmlist.last()
        return filmsToReturn
    }

    fun findFilmByName(filmName: String): MutableList<Film> {
        val filmsWithThisName = mutableListOf<Film>()
        for (film in filmlist){
            if (film.title == filmName) {
                filmsWithThisName += film
            }
        }
        return filmsWithThisName
    }



}
