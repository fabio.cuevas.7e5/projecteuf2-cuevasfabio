package test

import model.User
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import ui.filmitb

internal class FilmItbTest {

    @Test //Introduïr valors a una llista d'usuaris (username : String)
    fun adduserTest(){

        val userlist = filmitb.userlist

        assert(userlist.size == 0) //La llista té 0 usuaris guardats quan s'executa el programa
        userlist.add(model.User("Cristiano Ronaldo"))
        assert(userlist.size == 1) //Els usuaris són guardats de manera correcta i encara que el nom d'usuari siguin 2 paraules és detectat com a un únic usuari
    }


    @Test //Introduïr valors a una llista de "Films" (title : String, director : String, mainActor : String, genere : String, length : Int )
    fun addfilmTest(){

        val filmlist = filmitb.filmlist

        assert(filmlist.size == 0) //La llista té 0 "Films" guardades quan s'executa el programa
        filmlist.add(model.Film("Pel pel","Dir dir", "Act act", "Gen gen", 100))
        assert(filmlist.size == 1) //Les "Films" són guardades de manera correcta i encara que el títol, director, actor, genere siguin 2 paraules és detectat com a un únic usuari
    }

    @Test //Eliminar una "username" dins d'una llista d'usuaris (username : String)
    fun deleteuserTest(){
        val deleteuser = filmitb.userlist
        deleteuser.add(model.User("Lionel Messi")) //Tenim una llista d'usuaris que només té 1 usuari guardat
        assert(deleteuser.size == 1)
        deleteuser.removeAt(0) //Elimina l'usuari a l'index 0
        assert(deleteuser.size == 0)
    }
    @Test //Canvia el "username" per un altre (username : String)
    fun updateuserTest(){
        val updateuser = filmitb.userlist
        updateuser.add(model.User("Ronaldinho")) //Tenim una llista d'usuaris que només té 1 usuari guardat
        assert(updateuser[0]==User("Ronaldinho"))
        updateuser[0]= User("Mbappé")
        assert(updateuser[0]==User("Mbappé"))
    }

    @Test //Eliminar una "Film" dins d'una llista de "Films" (title : String, director : String, mainActor : String, genere : String, length : Int )
    fun deletefilmTest(){

        var deletefilm = filmitb.filmlist
        deletefilm.add(model.Film("Pel pel2","Dir dir2", "Act act2", "Gen gen2", 200))
        assert(deletefilm.size == 1) //Tenim una llista de "Films" que només té 1 "Film" guardada
        deletefilm.removeAt(0)
        assert(deletefilm.size == 0) //La llista és buida ja que hem eliminat la "Film" situada a l'index 0 de la llista
    }
}

