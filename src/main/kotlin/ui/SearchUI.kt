package ui


import java.util.*


class SearchUI (val sc: Scanner){

    fun showSearchMenu (){
        println("Search methods:\n" +
                "1: By title\n" +
                "2: By director\n" +
                "3: By main actor\n" +
                "4: By genere\n" +
                "5: By length\n" +
                "6: Not watched\n" +
                "7: Recomended\n" +
                "0: Return to main menu")
        chooseMenuSearch()
    }

    fun chooseMenuSearch (){
        val ui = UI()
        val optionSubmenu3 = sc.nextInt()
        when (optionSubmenu3){
            0 -> ui.showMenu()
            1 -> byTitle()
            2 -> byDirector()
            3 -> byActor()
            4 -> byGenere()
            5 -> byLength()

            7 -> recomended()
        }

    }

    private fun byTitle (){
        println("\nInput the title of the film:")
        sc.nextLine()
        val titlesearch = sc.nextLine()

        for (value in 0 .. filmitb.filmlist.lastIndex ){
            if (filmitb.filmlist[value].title == titlesearch ){
                println(filmitb.filmlist[value])
            }

        }
        showSearchMenu()
    }
    private fun byDirector (){
        println("\nInput the director of the film:")
        sc.nextLine()
        val directorsearch = sc.nextLine()

        for (value in 0 .. filmitb.filmlist.lastIndex ){
            if (filmitb.filmlist[value].director == directorsearch ){
                println(filmitb.filmlist[value])
            }

        }
        showSearchMenu()
    }
    private fun byActor(){
        println("\nInput the main actor of the film:")
        sc.nextLine()
        val actorsearch = sc.nextLine()

        for (value in 0 .. filmitb.filmlist.lastIndex ){
            if (filmitb.filmlist[value].mainActor == actorsearch ){
                println(filmitb.filmlist[value])
            }
        }
        showSearchMenu()
    }
    private fun byGenere(){
        println("\nInput the genere of the film:")
        sc.nextLine()
        val generesearch = sc.nextLine()

        for (value in 0 .. filmitb.filmlist.lastIndex ){
            if (filmitb.filmlist[value].genere == generesearch ){
                println(filmitb.filmlist[value])
            }
        }
        showSearchMenu()
    }
    private fun byLength(){
        println("\nInput the length of the film:")
        sc.nextLine()
        val lengthsearch = sc.nextInt()

        for (value in 0 .. filmitb.filmlist.lastIndex ){
            if (filmitb.filmlist[value].length == lengthsearch ){
                println(filmitb.filmlist[value])
            }
        }
        showSearchMenu()
    }
    fun recomended(){
        if (filmitb.filmlist.size >=5){
            println("Last 5 added films")
            println(filmitb.filmlist[filmitb.filmlist.lastIndex])
            println(filmitb.filmlist[filmitb.filmlist.lastIndex-1])
            println(filmitb.filmlist[filmitb.filmlist.lastIndex-2])
            println(filmitb.filmlist[filmitb.filmlist.lastIndex-3])
            println(filmitb.filmlist[filmitb.filmlist.lastIndex-4])
        }
        println("Last director films: ${filmitb.filmlist[filmitb.filmlist.lastIndex].director}")
        for (value in 0 .. filmitb.filmlist.lastIndex){
            if (filmitb.filmlist[value].director == filmitb.filmlist[filmitb.filmlist.lastIndex].director){
                println(filmitb.filmlist[value])
            }
        }
        println("Last main actor films: ${filmitb.filmlist[filmitb.filmlist.lastIndex].mainActor}")
        for (value in 0 .. filmitb.filmlist.lastIndex){
            if (filmitb.filmlist[value].mainActor == filmitb.filmlist[filmitb.filmlist.lastIndex].mainActor){
                println(filmitb.filmlist[value])
            }
        }
        println("Last genere films: ${filmitb.filmlist[filmitb.filmlist.lastIndex].genere}")
        for (value in 0 .. filmitb.filmlist.lastIndex){
            if (filmitb.filmlist[value].genere == filmitb.filmlist[filmitb.filmlist.lastIndex].genere){
                println(filmitb.filmlist[value])
            }
        }
        showSearchMenu()
    }
}