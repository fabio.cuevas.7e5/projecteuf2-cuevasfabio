package ui

import model.FilmItb
import ui.SearchUI
import java.util.*


fun main(){
    val start= UI()
    val filmitb = FilmItb()
    start.showMenu()
}



class UI {
    val sc= Scanner(System.`in`)

    fun showMenu(){
        println("Welcome to FilmItb:\n" +
                "1: User\n" +
                "2: Films\n" +
                "3: Search\n" +
                "0: Exit")
        chooseOption()
    }


    fun chooseOption(){
        val userUi = UserUI(sc)
        val filmUI = FilmUI(sc)
        val searchUI = SearchUI(sc)

        val option= sc.nextInt()
        when (option){
            1 -> userUi.showUserMenu()
            2 -> filmUI.showFilmMenu()
            3 -> searchUI.showSearchMenu()

        }
    }
}


