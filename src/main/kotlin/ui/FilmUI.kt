package ui

import model.Film
import model.FilmItb
import java.util.*
val filmitb = FilmItb()

class FilmUI (val sc: Scanner){


    fun showFilmMenu (){
        println("Films:\n" +
                "1: Add film\n" +
                "2: Show films\n" +
                "3: Delete films\n" +
                "4: Watch films\n" +
                "5: View watched films\n" +
                "6: Add film to favorites\n" +
                "7: Show favorites\n" +
                "8: Show likes per film\n" +
                "0: Return to main menu")
        chooseMenuFilm()
    }
    fun chooseMenuFilm(){
        val ui = UI()
        val optionSubmenu2 = sc.nextInt()
        when (optionSubmenu2){
            0 -> ui.showMenu()
            1 -> addfilm()
            2 -> showfilms()
            3 -> deletefilm()
            8 -> showlikes()
        }
    }
    fun addfilm(){
        sc.nextLine()
        println("\nTitle:")
        val filmName = sc.nextLine()

        println("\nInput the director of the film:")
        val directorName= sc.nextLine()

        println("\nInput the actor name:")
        val actorName = sc.nextLine()

        println("\nSelect the main genre of the movie:\n" +
                "1: Action / 2: Comedy / 3: Fantasy\n" +
                "4: Horror / 5: Mystery / 6: Romance\n" +
                "7: Thriller / 8: Western / 9: Suspense\n" +
                "10: Sci-fi / 11: War / 12: History\n" +
                "13: Animation / 14: Family / 15: Kids\n")

        val gender: String
        when (sc.nextLine().toInt()){
            1 -> gender = "Action"
            2 -> gender = "Comedy"
            3 -> gender = "Fantasy"
            4 -> gender = "Horror"
            5 -> gender = "Mystery"
            6 -> gender = "Romance"
            7 -> gender = "Thriller"
            8 -> gender = "Western"
            9 -> gender = "Suspense"
            10 -> gender = "Sci-fi"
            11 -> gender = "War"
            12 -> gender = "History"
            13 -> gender = "Animation"
            14 -> gender = "Family"
            15 -> gender = "Kids"

            else -> gender = "Other"
        }

        println("\nLength of the movie:")
        val duration = sc.nextInt()

        val film = Film(filmName, directorName, actorName, gender, duration)

        filmitb.filmlist.add(film)
        filmitb.filmItbStorage.save(filmitb.userlist, filmitb.filmlist)
        showFilmMenu()


    }
    fun showfilms(){
        println("\nShowing films:")
        for (value in 0 .. filmitb.filmlist.lastIndex ){
            println(filmitb.filmlist[value])
        }


        showFilmMenu()
    }

    fun deletefilm(){
        println("\nChoose film to delete:")
        filmitb.filmlist.removeAt(sc.nextInt())
        println("\nFilm deleted:")
        showFilmMenu()
    }

    fun showlikes(){
        println("There is not a like button implemented yet")
    }

}


